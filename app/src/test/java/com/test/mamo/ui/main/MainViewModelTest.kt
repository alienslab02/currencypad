package com.test.mamo.ui.main

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.google.common.truth.Truth.assertWithMessage
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainViewModelTest {
    private val viewModel = MainViewModel(ApplicationProvider.getApplicationContext())

    @Test
    fun `Check the number entered and displayed is correct`() {

        viewModel.onCreate()
        assertWithMessage("There should not be any number set number variable in state for the first time").that(viewModel.state.number).isEqualTo("")
        assertWithMessage("Should show 0.00").that(viewModel.state.formattedNumber).isEqualTo("0.00")
        viewModel.handleNumberClick(1)
        viewModel.handleNumberClick(2)
        viewModel.handleNumberClick(3)
        assertThat(viewModel.state.number).isEqualTo("123")
        assertWithMessage("Should show 123.00").that(viewModel.state.formattedNumber).isEqualTo("123.00")
        viewModel.handleDecimalClick()
        assertThat(viewModel.state.number).isEqualTo("123.")
        assertWithMessage("Should show 123.00").that(viewModel.state.formattedNumber).isEqualTo("123.00")
        viewModel.handleNumberClick(5)
        assertThat(viewModel.state.number).isEqualTo("123.5")
        assertWithMessage("Should show 123.50").that(viewModel.state.formattedNumber).isEqualTo("123.50")
        viewModel.handleNumberClick(8)
        assertThat(viewModel.state.number).isEqualTo("123.58")
        assertWithMessage("Should show 123.58").that(viewModel.state.formattedNumber).isEqualTo("123.58")

        // Remove All numbers
        viewModel.handleCrossClick()
        viewModel.handleCrossClick()
        viewModel.handleCrossClick()
        assertThat(viewModel.state.number).isEqualTo("123")
        assertWithMessage("Should show 123.00").that(viewModel.state.formattedNumber).isEqualTo("123.00")

        viewModel.handleCrossClick()
        viewModel.handleCrossClick()
        viewModel.handleCrossClick()
        assertThat(viewModel.state.number).isEqualTo("")
        assertWithMessage("Should show 0.00").that(viewModel.state.formattedNumber).isEqualTo("0.00")

    }

    @Test
    fun `Check the large number should be formatted`() {
        viewModel.onCreate()
        assertWithMessage("There should not be any number set number variable in state for the first time").that(viewModel.state.number).isEqualTo("")
        assertWithMessage("Should show 0.00").that(viewModel.state.formattedNumber).isEqualTo("0.00")
        viewModel.handleNumberClick(1)
        viewModel.handleNumberClick(2)
        viewModel.handleNumberClick(3)
        viewModel.handleNumberClick(4)
        assertThat(viewModel.state.number).isEqualTo("1234")
        assertThat(viewModel.state.formattedNumber).isEqualTo("1,234.00")
        viewModel.handleNumberClick(5)
        viewModel.handleNumberClick(6)
        viewModel.handleNumberClick(7)
        viewModel.handleNumberClick(8)
        assertThat(viewModel.state.number).isEqualTo("1234567")
        assertThat(viewModel.state.formattedNumber).isEqualTo("1,234,567.00")
        viewModel.handleDecimalClick()
        viewModel.handleNumberClick(4)
        viewModel.handleNumberClick(5)
        assertThat(viewModel.state.number).isEqualTo("1234567.45")
        assertThat(viewModel.state.formattedNumber).isEqualTo("1,234,567.45")
    }

}