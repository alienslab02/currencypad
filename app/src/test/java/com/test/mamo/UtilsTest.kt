package com.test.mamo

import org.junit.Test

import org.junit.Assert.*

class UtilsTest {

    @Test
    fun `Fixed Number should return a fixed decimal number upto the decimal places given in arguments (default 2)` () {
        assertEquals("Default decimal value should be 2",2, Utils.DECIMAL)
        assertEquals("Default number limit should be up to 7 digits ",7, Utils.NUMBER)
        assertEquals("12345", Utils.fixedNumber("12345"))
        assertEquals("12345", Utils.fixedNumber("12345", 3))
        assertEquals("12345.1", Utils.fixedNumber("12345.1", 3))
        assertEquals("12345.123", Utils.fixedNumber("12345.123", 3))
        assertEquals("12345.123", Utils.fixedNumber("12345.1234", 3))
        assertEquals("Should return number with 2 decimal places as default", "12345.12", Utils.fixedNumber("12345.1234"))
        assertEquals("Should show 2 decimal places", "12345.98", Utils.fixedNumber("12345.987"))
    }

    @Test
    fun `Trim the number` () {
        assertEquals("Should show first 3 digits", "123", Utils.trim("12345", 3))
        assertEquals("Should return the given number without trimming", "12345", Utils.trim("12345", 8))
    }
}