package com.test.mamo

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import java.text.DecimalFormat

object Utils {
    const val DECIMAL = 2;
    const val NUMBER = 7;

    fun spannableString(text: String, color: Int) =
        SpannableString(text).apply {
            setSpan(
                ForegroundColorSpan(color),
                0,
                length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

    fun trim(text: String, size: Int): String {
        var dec = text
        if (dec.length > size) dec = dec.substring(0, size)
        return dec
    }

    fun fixedNumber(num: String, decimals: Int = DECIMAL): String {
        if (num.contains(".")) {
            val splitted = num.split(".")
            return trim(splitted[0], NUMBER) + "." + trim(splitted[1], decimals)
        }
        return trim(num, NUMBER)
    }

    @Throws(NumberFormatException::class)
    fun currencyFormat(amount: String): String {
        val formatter = DecimalFormat("##,##0.00")
        return formatter.format(amount.toDouble())
    }

}