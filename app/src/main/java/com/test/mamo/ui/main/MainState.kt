package com.test.mamo.ui.main

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.test.mamo.BR
import com.test.mamo.Utils.currencyFormat
import com.test.mamo.Utils.fixedNumber

class MainState : BaseObservable() {

    @get:Bindable
    var number: String = ""
        set(value) {
            field = fixedNumber(value)
            notifyPropertyChanged(BR.number)
            formatDisplayNumber()
        }

    @get:Bindable
    var activeEndIndex: Int = -1
        set(value) {
            field = value
            notifyPropertyChanged(BR.activeEndIndex)
        }

    @get:Bindable
    var currency: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.currency)
        }

    @get: Bindable
    var formattedNumber: String = "0"
        set(value) {
            field = value
            notifyPropertyChanged(BR.formattedNumber)
        }

    fun reset() {
        number = ""
        currency = "AED"
        activeEndIndex = -1
    }


    @Throws(NumberFormatException::class)
    private fun formatDisplayNumber() {
        val fn = when {
            number.isEmpty() -> "0"
            else -> number
        }
        formattedNumber = currencyFormat(fn)
        calculateEnabledIndex()
    }

    /**
     * Calculates the index up to which the selection should be of black color and the rest will be
     * gray
     */
    private fun calculateEnabledIndex() {
        activeEndIndex = when {
            number.isEmpty() -> -1
            number.contains(".")
            -> {
                val ind1 = formattedNumber.split(".")[0].length - 1
                val ind2 = number.split(".")[1].length
                ind1 + ind2 + 1
            }
            else -> formattedNumber.split(".")[0].length - 1
        }
    }
}