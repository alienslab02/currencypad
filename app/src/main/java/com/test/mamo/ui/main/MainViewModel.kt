package com.test.mamo.ui.main

import android.app.Application
import com.test.mamo.base.BaseViewModel

class MainViewModel(application: Application) : BaseViewModel(application) {
    val state: MainState = MainState()

    override fun onCreate() {
        super.onCreate()
        state.reset()
    }

    fun handleNumberClick(number: Int) {
        try {
            state.number = state.number + number
        } catch (ex: NumberFormatException) {
            // Let's remove the last added digit
            handleCrossClick()
            println("The given string is non-numeric")
        }
    }

    fun handleDecimalClick() {
        try {
            state.number = state.number + "."
        } catch (ex: NumberFormatException) {
            // Let's remove the last added digit
            handleCrossClick()
            println("The given string is non-numeric")
        }
    }

    fun handleCrossClick() {
        try {
            state.number = if (state.number.length > 1) state.number.substring(
                0,
                state.number.length - 1
            ) else ""
        } catch (ex: NumberFormatException) {
            println("The given string is non-numeric")
        }
    }
}