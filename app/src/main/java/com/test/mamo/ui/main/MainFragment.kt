package com.test.mamo.ui.main

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.test.mamo.Utils
import com.test.mamo.databinding.MainFragmentBinding


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()

        @JvmStatic
        @BindingAdapter("text", "currency", "activeEnd")
        fun format(textView: TextView, text: String, currency: String, activeEnd: Int) {
            val end = if(activeEnd >= text.length) text.length else activeEnd + 1
            val text1 = if (end < 0) "" else text.substring(0, end)
            val text2 = text.substring(end, text.length)

            val builder = SpannableStringBuilder()

            if (end == 0) {
                // All gray
                Utils.spannableString("$currency ", Color.GRAY).also {
                    builder.append(it)
                }
            } else {
                Utils.spannableString("$currency ", Color.BLACK).also {
                    builder.append(it)
                }
            }

            Utils.spannableString(text1, Color.BLACK).also {
                builder.append(it)
            }

            Utils.spannableString(text2, Color.GRAY).also {
                builder.append(it)
            }

            textView.setText(builder, TextView.BufferType.SPANNABLE)
        }
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        this.lifecycle.addObserver(viewModel)
        return MainFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
            lifecycleOwner = this@MainFragment
        }.root
    }

    override fun onDestroy() {
        super.onDestroy()
        this.lifecycle.removeObserver(viewModel)
    }

}