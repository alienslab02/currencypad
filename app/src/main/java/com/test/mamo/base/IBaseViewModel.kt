package com.test.mamo.base

import androidx.lifecycle.LifecycleObserver

interface IBaseViewModel: LifecycleObserver {
    fun onCreate();
    fun onStart();
    fun onResume();
    fun onPause();
    fun onDestroy();
}